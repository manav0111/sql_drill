use mountblue;

CREATE TABLE IF NOT EXISTS Prescription(
    Prescription_id integer primary key,
    Drug text,
    Date date,
    Dosage integer,
    Doctor_name text,
    Secretory_name text
);

INSERT INTO Prescription
VALUES (1,'paracetamol','2024-01-23',1,'manav goyal','nikhil');

CREATE TABLE IF NOT EXISTS Patient(
    id integer primary key,
    name text,
    DOB text,
    Address text,
    Prescription_id integer,
    Foreign Key (Prescription_id) REFERENCES  Prescription(Prescription_id)
);

INSERT INTO Patient
VALUES (10,'manas','2004-03-28','haridwar',1);


SELECT * from Patient;