use mountblue;

show tables;



CREATE TABLE IF NOT EXISTS Doctor(
    id integer primary key,
    Patient_id integer,
    Foreign Key (Patient_id) REFERENCES  Patient(id),
    Prescription_id integer,
    Foreign Key (Prescription_id) REFERENCES  Prescription(Prescription_id)
);


INSERT INTO Doctor
VALUES (100,10,1);

SELECT *from Doctor;
