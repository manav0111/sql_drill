show databases;

create database mountblue;

use mountblue;

create table if not exists BRANCH(
branch_id int primary key auto_increment,
branch_addr text
);

INSERT INTO BRANCH 
VALUES (1,"HARIDWAR"),
        (2,"DEHRADUN");


CREATE TABLE IF NOT EXISTS BOOK(
ISBN INT PRIMARY KEY,
Title text,
Author text,
Publisher text,
Num_copies integer,
branch_id integer,
FOREIGN KEY (branch_id) REFERENCES BRANCH(branch_id)
);

 INSERT INTO BOOK
 VALUES (1,'NEVER GIVE UP','MANAV GOYAL','FAIZAN',10,1),
        (2,'BELIVE IN YOURSELF','ARYAN','MANAV',100,2);



SELECT * FROM BOOK;